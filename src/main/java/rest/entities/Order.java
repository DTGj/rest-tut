package rest.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import rest.utils.Status;

@Data
@NoArgsConstructor
@Entity
@Table(name = "CUSTOMER_ORDER")
public class Order {
	private @Id @GeneratedValue Long id;
	
	private String description;
	private Status status;
	
	public Order(String description, Status status) {
		this.description = description;
		this.status = status;
	}

	@Override
	public String toString() {
		return "Order [id orden=" + id + ", description=" + description + ", status=" + status + "]";
	}
	
	
}
