package rest.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@Entity
public class Employee {
	
	private @Id @GeneratedValue Long id;
	String firtsName;
	String lastName;
	String role;
	
	
	public Employee(String firtsName, String lastName, String role) {
		super();
		this.firtsName = firtsName;
		this.lastName = lastName;
		this.role = role;
	}


	@Override
	public String toString() {
		return "Employee [id=" + id + ", firtsName=" + firtsName + ", lastName=" + lastName + ", role=" + role + "]";
	}
	
	public String getName() {
		return this.firtsName + " " + this.lastName;
	}
	
	public void setName(String name) {
		String[] parts = name.split(" ");
		this.firtsName = parts[0];
		this.lastName = parts[1];
	}
	
	
	
}
