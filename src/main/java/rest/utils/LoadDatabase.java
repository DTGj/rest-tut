package rest.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import rest.entities.Employee;
import rest.entities.Order;
import rest.repository.EmployeeRepository;
import rest.repository.OrderRepository;

@Configuration
public class LoadDatabase {
	
	private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);
	
	@Bean
	CommandLineRunner initDatabase(EmployeeRepository employeeRepository, OrderRepository orderRepository) {
		
		return  args -> {
			log.info("Preloading"  +employeeRepository.save(new Employee( "Bilbo", "Baggins","ladron ")));
			log.info("Preloading"  +employeeRepository.save(new Employee( "Frodo", "Baggins","ladroncillo ")));
			log.info("Preloading"  +orderRepository.save(new Order( "Orden de compra champu",Status.IN_PROGRESS)));
			log.info("Preloading"  +orderRepository.save(new Order( "Orden de compra champcitu",Status.IN_PROGRESS)));
		};
	}
}
