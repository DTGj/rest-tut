package rest.utils;

public enum Status {
	IN_PROGRESS,
	COMPLETED,
	CANCELLED;
}
