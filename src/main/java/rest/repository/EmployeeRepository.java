package rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rest.entities.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
