package rest.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import rest.entities.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
	
}
