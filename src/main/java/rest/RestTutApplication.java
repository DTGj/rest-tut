package rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestTutApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestTutApplication.class, args);
	}
	
}
